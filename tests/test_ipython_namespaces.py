from ipython_namespaces.namespaces import FallbackDict


def test_fallback_dict():
    fallback_dict = FallbackDict({"foo": 23})
    fallback_dict["bar"] = 42
    assert fallback_dict["foo"] == 23
    assert fallback_dict["bar"] == 42
